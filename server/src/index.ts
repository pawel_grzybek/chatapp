import debug from "debug";
import express from "express";
import http from "http";
import path from "path";
import { IoServer } from "./IoServer";

const INFO = debug("INFO:index");
const DEBUG = debug("DEBUG:index");

const app = express();

const staticPath = path.join(__dirname, "../../static");
DEBUG("staticPath:", staticPath);
app.use(express.static(staticPath));

const httpServer = http.createServer(app);
// @ts-ignore
const ioServer = new IoServer(httpServer);

httpServer.listen(4000, () => {
  INFO(`Server started at port 4000`);
});
