type Message = Readonly<{
  username: string;
  message: string;
  createdAt: number;
}>;

export function createMessage(username: string, message: string): Message {
  return {
    username,
    message,
    createdAt: new Date().getTime()
  };
}

export class MessageStore {
  private store: Message[] = [];

  public add = (message: Message) => this.store.push(message);
  public getAll = (): Message[] => this.store;
}
