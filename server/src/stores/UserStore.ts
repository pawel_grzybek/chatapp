export type User = Readonly<{
  id: string;
  username: string;
}>;

export class UserStore {
  private users: User[] = [];

  public add = ({ id, username }: User): User => {
    const cleanedUsername = username.trim().toLowerCase();

    if (!cleanedUsername) {
      throw new Error("Username and room are required");
    }

    if (this.users.find((user) => user.username === cleanedUsername)) {
      throw new Error("Username is in use!");
    }

    const newUser = { id, username: cleanedUsername };
    this.users.push(newUser);
    return newUser;
  };

  public remove = (id: string): User | undefined => {
    const index = this.users.findIndex((u) => u.id === id);
    return index !== -1 ? this.users.splice(index, 1)[0] : undefined;
  };

  public get = (id: string): User | undefined => this.users.find((u) => u.id === id);
}
