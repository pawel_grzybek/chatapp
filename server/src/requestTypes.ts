export interface MessageRequest {
  username: string;
  message: string;
}

export interface UserJoinRequest {
  username: string;
}

export const isMessageRequestType = (req: unknown): req is MessageRequest => {
  const msgReq = req as MessageRequest;
  return (
    msgReq?.username !== undefined &&
    msgReq?.message !== undefined &&
    typeof msgReq.username === "string" &&
    typeof msgReq.message === "string"
  );
};

export const isUserJoinRequestType = (req: unknown): req is UserJoinRequest => {
  const joinReq = req as UserJoinRequest;
  return joinReq?.username !== undefined && typeof joinReq.username === "string";
};
