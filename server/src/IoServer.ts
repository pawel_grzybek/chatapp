import BadwordsFilter from "bad-words";
import debug from "debug";
import http from "http";
import socketio, { Server } from "socket.io";
import { isMessageRequestType, isUserJoinRequestType } from "./requestTypes";
import { createMessage, MessageStore } from "./stores/MessageStore";
import { UserStore } from "./stores/UserStore";

const INFO = debug("INFO:IoServer");
const ERROR = debug("ERROR:IoServer");

const filter = new BadwordsFilter();
const ROOM = "default";
export class IoServer {
  private io: Server;
  private messageStore = new MessageStore();
  private userStore = new UserStore();

  constructor(httpServer: http.Server) {
    this.io = new Server(httpServer, {
      cors: {
        origin: "*",
        methods: ["GET", "POST"]
      }
    });

    this.io.on("connection", (socket: socketio.Socket) => {
      INFO("New web socket connection");

      socket.on("join", (req: unknown, ack: unknown) => this.onJoin(socket, req, ack));
      socket.on("message", (req: unknown, ack: unknown) => this.onMessage(socket, req, ack));
      socket.on("disconnect", () => this.disconnect(socket));
    });
  }

  private onJoin = (socket: socketio.Socket, req: unknown, ack: unknown) => {
    INFO("New user joined:", req);

    if (isUserJoinRequestType(req)) {
      const { username } = req;

      try {
        this.userStore.add({ id: socket.id, username });
      } catch (error) {
        this.handleAck(ack, "onJoin", error);
      }

      socket.join(ROOM);
      socket.emit("messagelist", this.messageStore.getAll(), this.handleResponse("message-list"));
      this.handleAck(ack, "onJoin", "Joined");
    }
  };

  private onMessage = (socket: socketio.Socket, req: unknown, ack: unknown) => {
    INFO("Message received:", req);
    if (isMessageRequestType(req)) {
      const { username, message } = req;

      const user = this.userStore.get(socket.id);
      if (!user) {
        INFO("No such user:", username);
        this.handleAck(ack, "onMessage", "No such user");
        return;
      }

      const msg = createMessage(username, filter.clean(message));
      this.messageStore.add(msg);
      this.io.to(ROOM).emit("message", msg);
      this.handleAck(ack, "onMessage", "Received");
    }
  };

  private disconnect = (socket: socketio.Socket): void => {
    const user = this.userStore.remove(socket.id);
    INFO("Client disconnected:", user?.username);
  };

  private handleResponse = (tag: string) => (msg: string) => {
    INFO(`${tag}: Response: ${msg}`);
  };

  private handleAck = (ack: unknown, tag: string, message: string) => {
    try {
      if (typeof ack === "function") {
        ack(message);
      }
    } catch (error) {
      ERROR(`${tag}:`, error);
    }
  };
}
