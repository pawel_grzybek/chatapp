// eslint-disable-next-line @typescript-eslint/no-var-requires
const path = require("path");

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

module.exports = {
  preset: "ts-jest",
  testPathIgnorePatterns: ["/lib/", "out", "/node_modules/"],
  moduleFileExtensions: ["js", "ts", "tsx", "json", "node"],
  collectCoverage: false,
  collectCoverageFrom: [
    "**/*.{ts,tsx}",
    "!**/*.d.{js,ts}",
    "!**/coverage/**",
    "!**/out-test/**",
    "!**/lib/**",
    "!**/gen/**",
    "!**/examples/**",
    "!**/sample/**",
    "!**/test/**",
    "!**/scripts/**"
  ],
  reporters: ["default", "jest-junit"],
  coverageReporters: ["json", "lcov", "text-summary", "html-spa", "cobertura"],
  moduleNameMapper: {
    "\\.svg$": path.resolve(__dirname, "./mocks/fileMock.js")
  },
  setupFilesAfterEnv: []
};
