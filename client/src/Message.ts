
export type Message = Readonly<{
  username: string;
  message: string;
  createdAt: number;
}>;

export const isMessage = (obj: unknown): obj is Message => {
  const msg = obj as Message;
  return msg.message !== undefined && msg.username !== undefined;
}

export const isMessageList = (obj: unknown): obj is Message[] => {
  const list = obj as Message[];
  return list.every(isMessage);
}