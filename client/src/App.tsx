import React, { useCallback, useEffect, useState } from "react";
import socketIOClient, { Socket } from "socket.io-client";
import { DefaultEventsMap } from "socket.io-client/build/typed-events";
import "./App.scss";
import { InputArea, MessageList, NameSelector } from "./components";
import { isMessage, isMessageList, Message } from "./Message";

const ENDPOINT = "http://127.0.0.1:4000";

export const App: React.FC = (): JSX.Element => {
  const [socket, setSocket] = useState<Socket<DefaultEventsMap, DefaultEventsMap>>();
  const [userName, setUserName] = useState<string>("");
  const [messages, setMessages] = useState<Message[]>([]);

  const sendMessage = useCallback(
    (message) => {
      console.log("Sending ...");
      if (socket && userName && message) {
        socket.emit("message", { username: userName, message }, (resp: string) => {
          console.log(`Response: ${resp}`);
        });
        console.log("Sent");
      }
    },
    [socket, userName]
  );

  const onMessagesReceived = useCallback((list: unknown, ack: unknown) => {
    if (isMessageList(list) && typeof ack === "function") {
      setMessages(list);
      ack("Received");
    }
  }, []);

  const onDisconnect = useCallback(() => {
    console.log("Connection to server lost...");
    setUserName("");
  }, []);

  const onMessageReceived = useCallback((msg: unknown) => {
    if (isMessage(msg)) {
      setMessages((prev) => [...prev, msg]);
    }
  }, []);

  useEffect(() => {
    const s = socketIOClient(ENDPOINT);
    s.on("messagelist", onMessagesReceived);
    s.on("message", onMessageReceived);
    s.on("disconnect", onDisconnect);

    setSocket(s);
    return () => {
      s?.disconnect();
    };
  }, [onMessageReceived, onMessagesReceived, onDisconnect]);

  useEffect(() => {
    if (socket && userName) {
      socket.emit("join", { username: userName }, (response: string) => {
        if (response !== "Joined") {
          setUserName("");
          alert("Username already in use");
        }
      });
    }
  }, [socket, userName]);

  return (
    <>
      {!userName ? (
        <NameSelector onSubmit={setUserName} />
      ) : (
        <main className="app-main">
          <MessageList messages={messages} />
          <InputArea onSubmit={sendMessage} />
        </main>
      )}
    </>
  );
};

export default App;
