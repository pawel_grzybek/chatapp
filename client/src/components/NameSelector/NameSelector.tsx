import React, {
  useCallback,
  useEffect,
  useRef,
  useState
  } from "react";
import "./NameSelector.scss";

interface NameSelectorProps {
  onSubmit: (username: string) => void;
  initialValue?: string;
}

export const NameSelector: React.FC<NameSelectorProps> = ({ initialValue, onSubmit }): JSX.Element => {
  const [name, setName] = useState(initialValue || "");
  const textInputRef = useRef<HTMLInputElement | null>(null);

  useEffect(() => {
    const el = textInputRef.current;
    if (el) {
    }
    el?.focus();
  });

  const handleSubmit = useCallback(() => {
    if (!!name) {
      onSubmit(name);
    }
  }, [name, onSubmit]);

  const handleChange = useCallback((event: React.ChangeEvent<HTMLInputElement>): void => {
    setName(event.target.value);
  }, []);

  const handleKeyDown = useCallback(
    (event: React.KeyboardEvent<HTMLInputElement>): void => {
      if (event.key === "Enter" && !!name) {
        onSubmit(name);
      }
    },
    [name, onSubmit]
  );

  return (
    <div className="name-selector-backdrop">
      <div className="name-selector-popup">
        <h1 className="name-selector-title">Enter username</h1>
        <input
          ref={textInputRef}
          className="name-selector-input"
          type="text"
          placeholder="Display name"
          value={name}
          onChange={handleChange}
          onKeyDown={handleKeyDown}
        />
        <button className="name-selector-button" onClick={handleSubmit} disabled={!name}>
          Join
        </button>
      </div>
    </div>
  );
};
