import React, {
  useCallback,
  useEffect,
  useRef,
  useState
  } from "react";
import "./InputArea.scss";

interface InputAreaProps {
  onSubmit: (username: string) => void;
}

export const InputArea: React.FC<InputAreaProps> = ({ onSubmit }): JSX.Element => {
  const [message, setMessage] = useState<string>("");
  const textInputRef = useRef<HTMLInputElement | null>(null);

  useEffect(() => {
    const el = textInputRef.current;
    if (el) {
    }
    el?.focus();
  });

  const handleSubmit = useCallback(() => {
    if (!!message) {
      onSubmit(message);
      setMessage("");
    }
  }, [message, onSubmit]);

  const handleChange = useCallback((event: React.ChangeEvent<HTMLInputElement>): void => {
    setMessage(event.target.value);
  }, []);

  const handleKeyDown = useCallback(
    (event: React.KeyboardEvent<HTMLInputElement>): void => {
      if (event.key === "Enter" && !!message) {
        onSubmit(message);
        setMessage("");
      }
    },
    [message, onSubmit]
  );

  return (
    <div className="input-area">
      <input
        ref={textInputRef}
        className="input-area-input"
        type="text"
        placeholder="Enter message here ..."
        value={message}
        onChange={handleChange}
        onKeyDown={handleKeyDown}
      />
      <button className="input-area-button" onClick={handleSubmit} disabled={!message}>
        Send
      </button>
    </div>
  );
};
