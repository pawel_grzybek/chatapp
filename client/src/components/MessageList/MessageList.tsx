import React, { useEffect, useRef } from "react";
import { Message } from "../../Message";
import { MessageLine } from "./MessageLine";
import "./MessageList.scss";

interface MessageListProps {
  messages: Message[];
}

export const MessageList: React.FC<MessageListProps> = ({messages}): JSX.Element => {
  const divRef = useRef<HTMLDivElement | null>(null);

  useEffect(()=> {
    const el = divRef.current;
    if(el) {
      const scrollHeight = el.scrollHeight;
      const currHeight = el.clientHeight;
      const maxScroll = scrollHeight - currHeight;
      el.scrollTop = maxScroll > 0 ? maxScroll : 0;
    }
  })
  
  return (
    <div className="message-list" ref={divRef}>
      {
      messages.map((msg, index) =>  (
        <MessageLine 
          key={index}
          message={msg}
          dark={index % 2 === 1}
          />
        )
      )}
    </div>);
}