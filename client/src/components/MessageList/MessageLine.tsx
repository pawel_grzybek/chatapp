import React from "react";
import { Message } from "../../Message";
import "./MessageLine.scss";

interface MessageLineProps {
  message: Message;
  dark?: boolean;
}

const LOCALE = "en-gb";

export const MessageLine: React.FC<MessageLineProps> = ({message: msg, dark}): JSX.Element => {
  const {message, username} = msg;

  const createdAt = new Date(msg.createdAt);
  const createdTime = createdAt.toLocaleTimeString(LOCALE);
  const createdDate = createdAt.toLocaleDateString(LOCALE);

  const shouldShowDate = new Date().toLocaleDateString(LOCALE).localeCompare(createdDate) !== 0;
  
  return (
    <div className={`message-line ${dark ? "message-line-dark" : ""}`}>
      <p>
        <span className="message-name">{username}</span>
        <span className="message-time">{shouldShowDate ? `${createdDate} ` : ""}{createdTime}</span>
      </p>
      <p>{message}</p>
    </div>
  );
}