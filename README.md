To run:

1. go into "client" folder and execute:
- yarn && yarn build
2. go into "server" folder and execute:
- yarn && yarn build && yarn start
3. open "localhost:4000"

